---
layout: page
title: Development
permalink: /development/
sitemap: true
---

Source code is managed using Git. You can browse it on [Gitlab](https://invent.kde.org/network/falkon) or clone it locally using:

{% highlight bash %}
git clone https://invent.kde.org/network/falkon.git
{% endhighlight %}

### Contributing

Code review is done on [Gitlab](https://invent.kde.org/network/falkon/-/merge_requests).

### Contact

You can get in contact with developers using [mailing list](https://mail.kde.org/mailman/listinfo/falkon) or IRC [#falkon over Libera Chat](irc://irc.libera.chat/falkon).

### Reporting bugs

You can report any bugs or feature request in [KDE Bugzilla](https://bugs.kde.org/enter_bug.cgi?product=Falkon). Before reporting, please make sure your issue isn't already reported ([open issues](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&component=extensions&component=general&list_id=1506993&product=Falkon)).
