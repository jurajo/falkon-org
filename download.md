---
layout: page
title: Download
permalink: /download/
sitemap: true
---

Source code and official binaries for stable releases can be downloaded at [download.kde.org](https://download.kde.org/stable/falkon/).

### Windows

Windows 7 is minimum supported Windows version.

You can choose between Standard (system-wide) installation and Portable installation which only extracts files
and setup the application to run in portable mode by default.

[Falkon-3.1.0.exe](https://download.kde.org/stable/falkon/3.1/Falkon.Installer.3.1.0.exe) - for 32-bit Windows

[Falkon-3.1.0-x64.exe](https://download.kde.org/stable/falkon/3.1/Falkon.Installer.3.1.0.x64.exe) - for 64-bit Windows

### Linux

You should install `falkon` package from your distribution's repositories.

#### Flatpak

Nightly builds are available in `kdeapps` flatpak repository.

[Flatpak Install]({{ "/falkon.flatpakref" | prepend: site.baseurl }})

{% highlight bash %}
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists kdeapps --from https://distribute.kde.org/kdeapps.flatpakrepo
flatpak install kdeapps org.kde.falkon
flatpak run org.kde.falkon
{% endhighlight %}

#### Snap

<a href="https://snapcraft.io/falkon">Falkon</a> releases can be installed using Snap on any Linux distro

[Install Falkon Snap](snap://falkon)

{% highlight bash %}
sudo snap install falkon
{% endhighlight %}

[Get set up for Snaps](https://docs.snapcraft.io/core/install)
