---
layout: post
title:  "State of MS Windows support"
date:   2024-02-12 12:00:00 +0000
comments: true
---

## Introduction

Hello, My name is Juraj and I am the current maintainer of Falkon. I
prefer free open source software and I am of an opinion that the
developers / programmers should not package their software, publishing
the source code is enough, and packaging should be left to people who
want to make the program available on the platform of their choice.

## Compilation

The code as it is now probably cannot be compiled due to recent
changes in the required version of ECM (Extra CMake Modules) version.
See this
[commit](https://invent.kde.org/network/falkon/-/commit/eb5b015a5d86aec0aa99f1fec08d7a1b28d8cf5f)
as a reference for the required changes. This breakage was not
intentional, I just do not use MS Windows for development and there is
no working CI to make sure all code compiles properly.

## Packaging

This probably also means the installer, some old configuration of it
is present in Falkon
[source code](https://invent.kde.org/network/falkon/-/tree/master/windows?ref_type=heads)
but I have no way to tell if it is good or bad.

## Craft

This is where the Craft can be used.

From what I have read, this is the best method how to set it up and
also what is the CI using the run its builds.

So got to the KDE Wiki and check KDE blogs:
- [Craft](https://community.kde.org/Craft)
- [Start crafting on Windows](https://community.kde.org/Get_Involved/development/Windows)
- [Farewell, Binary Factory! Add Craft Jobs for Your Apps to KDE's GitLab Now ](https://blogs.kde.org/2024/01/30/farewell-binary-factory-add-craft-jobs-your-apps-kdes-gitlab-now)

And than update and maintain the craft
[blueprint for Falkon](https://invent.kde.org/packaging/craft-blueprints-kde/-/blob/master/kde/unreleased/falkon/falkon.py?ref_type=heads).
As can be seen Falkon is in an "unreleased" section, it needs to be
tested and moved to the stable / released section.

## MS Windows specific features

I do not have interest to support this platform on my own, nor do I
want to intentionally harm it. I try to maintain current features when
there is a bug report from whatever platform. I personally do not plan
to add any specific features for this platform, but if the library
supports it I will not restrict it either.

## Conclusion

Falkon releases for MS Windows (and probably also Apple Mac) is
waiting for someone who wants to take care of it.

Feel free to contact me at Falkon mailing list, I will try to help if
I think I know.

Best regards,  
Juraj

