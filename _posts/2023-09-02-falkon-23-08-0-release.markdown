---
layout: post
title:  "Falkon 23.08.0 released"
date:   2023-09-02 12:00:00 +0000
comments: true
---

New Falkon version 23.08.0 is being released as part of KDE Gear.

### Notable changes

#### Zoom indicator to the AddressBar

When the zoom level on the page is different than the default, show
current zoom level in the address bar.

<figure class="image">
  <img src="/images/2023-09-02_falkon-zoom-label-in-address-bar.png" alt="Address bar with zoom label">
  <figcaption>Address bar with zoom label</figcaption>
</figure>


#### Expand address bar suggestion popup to the window width

It is possible to widen the address bar suggestion popup to the window
width. This option is disabled by default, but it can be useful on
smaller screens.

<figure class="image">
  <img src="/images/2023-09-02_falkon-expanded-address-bar-suggestion-popup.png" alt="Expanded address bar suggestion popup">
  <figcaption>Expanded address bar suggestion popup</figcaption>
</figure>


#### Permanent certificate exceptions

Certificate exceptions can now be stored indefinitely. There is also
a GUI to see and manage the current exceptions which is located at
[Preferences > Other > Certificate Exception Manager]

Contributed by Javier Llorente.

<figure class="image">
  <img src="/images/2023-09-02_falkon-certificate-exception-manager.png" alt="Certificate exception Manager">
  <figcaption>Certificate exception manager</figcaption>
</figure>


### Changelog

* A bit faster restoring of session with a lot of tabs
* Add support for custom URI schemes (BUG: 434099)
* Add CMake option "BUILD_PYTHON_SUPPORT" to enable/disable Python support
* Add zoom indicator to the addressbar (BUG: 399001)
* Add an option to expand addresbBar suggestion popup to the window width
* Implement a GUI for managing ignored SSL hosts (by Javier Llorente)
* Add KDE branding bookmarks and speeddial entries (By Javier Llorente)
* Implement download integration with Plasma (By Javier Llorente)


**Download**: [ffalkon-23.08.0.tar.xz](https://download.kde.org/stable/release-service/23.08.0/src/falkon-23.08.0.tar.xz) ([sig](https://download.kde.org/stable/release-service/23.08.0/src/falkon-23.08.0.tar.xz.sig) signed with [EBC3FC294452C6D8](/signing-keys/esk-riddell.gpg))
