---
layout: post
title:  "Falkon 3.0.1 released"
date:   2018-05-08 15:22:48 +0100
comments: true
---
First bugfix release is now out.

#### Binary builds

Starting with this release, there are official builds available for Linux and Windows platforms.

In addition to nightly Linux Flatpak builds, there is now also AppImage for stable releases.

For Windows, there are 32 and 64-bit installers, which now includes both standard and portable versions.

State of macOS port is currently in bad shape, so there won't be any macOS builds until the situation changes.

Builds are available on [Download](/download) page.

#### Changelog

* added profile migration from QupZilla
* fix incorrectly completing form data on some sites [#391327](https://bugs.kde.org/391327)
* fix showing incorrect state of back and forward buttons [#391331](https://bugs.kde.org/391331)
* fix loading urls containing %20 from location bar [#392445](https://bugs.kde.org/392445)
* fix save page action for downloading text files [#391300](https://bugs.kde.org/391300)
* fix download manager trying to closing not restored tabs [#392565](https://bugs.kde.org/392565)
* fix updating location bar site icon after using paste and go action [#392747](https://bugs.kde.org/392747)
* fix various issues with QtWebEngine 5.11 [#392885](https://bugs.kde.org/392885), [#393398](https://bugs.kde.org/393398)
* fix speed dial sometimes loading with no pages [#391875](https://bugs.kde.org/391875)
* fix leaving html fullscreen after closing tab [#393797](https://bugs.kde.org/393797)
* fix saving speed dial settings when there are no configured pages
* fix restoring nested crashed sessions
* fix setting minimum height of bookmarks toolbar
* VerticalTabs: fix preserving expanded state of times inside collapsed parent [#393567](https://bugs.kde.org/393567)
* VerticalTabs: don't force open sidebar when opening new window [#393629](https://bugs.kde.org/393629)

**Download**: [falkon-3.0.1.tar.xz](http://download.kde.org/stable/falkon/3.0.1/falkon-3.0.1.tar.xz) ([sig](http://download.kde.org/stable/falkon/3.0.1/falkon-3.0.1.tar.xz.sig) signed with [EBC3FC294452C6D8](https://sks-keyservers.net/pks/lookup?op=vindex&search=0xEBC3FC294452C6D8))
