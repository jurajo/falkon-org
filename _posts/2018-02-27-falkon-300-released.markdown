---
layout: post
title:  "Falkon 3.0.0 released"
date:   2018-02-27 20:04:30 +0100
comments: true
---
Falkon is a new KDE web browser, previously known as QupZilla. Following this release, there will only be one last final QupZilla release.

If you were previously using QupZilla, you can manually migrate your profiles to Falkon by moving the config directory (usually in `~/.config/qupzilla/`).
There is no automatic migration.

Apart from some under the hood changes, like changing build system from qmake to CMake or using XDG paths, there aren't many differencies from QupZilla.
There is a new extension - Vertical Tabs, but it will also be available later this week in last QupZilla release.

It is possible to write Falkon extensions in Python, but since the bindings are generated using PySide2 which is not yet released and thus not packaged by distributions, it won't be currently available for users.

Windows and macOS support is not yet finished, it will be available later.

**Download**: [falkon-3.0.0.tar.xz](http://download.kde.org/stable/falkon/3.0/src/falkon-3.0.0.tar.xz) ([sig](http://download.kde.org/stable/falkon/3.0/src/falkon-3.0.0.tar.xz.sig) signed with [EBC3FC294452C6D8](https://sks-keyservers.net/pks/lookup?op=vindex&search=0xEBC3FC294452C6D8))
